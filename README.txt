Rebecca Wernis and Scott Yantek
CS 2 Weeks 9 & 10
Othello


Group member contributions:
Scott: framework for doMove and a primitive version of doMove.
Becca: constructor, improvements and bug fixes in doMove, getScore, and
 movePositionMod
 
 
AI Improvements for tournament:
Our AI uses a simple heuristic involving score multipliers to determine the
next move it makes. Only the next move is considered. The base score is the number
of pieces taken in the move. The base score is multiplied by applicable score
multipliers, which can be >1 if they are good or <1 if they are bad. Moves that
capture a corner have the highest multipliers, followed by those that give a
spot on an edge not adjacent to a corner. Central squares not diagonally adjacent
to corners are the next best. Bad scores are given to edges adjacent to corners,
and even worse scores are given to squares that are diagonally adjacent to corners.

