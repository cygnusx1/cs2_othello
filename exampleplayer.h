#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__
#define MINSCORE -1
// multipliers for scoring corner, edge, and diagonally-adjacent-to-corner moves
#define CORNER_MULT 3
#define MID_EDGE_MULT 2
#define CORNER_ADJ_EDGE_MULT 0.5
#define DIAG_ADJ_CORNER_MULT 0.2

#include <iostream>
#include "common.h"
#include "board.h"
#include <time.h>
using namespace std;

class ExamplePlayer {
private:
    Side side;
    Board* board;
    float getScore(Move *possibleMove);
public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
