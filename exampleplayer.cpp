#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {     
     // initialize board
     Board* board = new Board();
     
     // make the pointer to the board a property of the ExamplePlayer class
     this->board = board;
     
     // make our side a property of the ExamplePlayer class
     this->side = side;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer()
{
    delete this->board;
}

/* This function modifies possibleScore based on whether possibleMove is to a
 * particularly advantageous or disadvantageous spot. In summary, moves to
 * corners are great, moves to edges not next to corners are good, moves to
 * edges next to corners are kinda bad, and moves to one of the squares
 * diagonally adjacent to a corner are very bad.
 *
 * Since this function is only called by getScore(), it need not be a member
 * function of the ExamplePlayer class.
 */
float movePositionMod(Move* possibleMove)
{
    if ((possibleMove->getX() == 0) ||
        (possibleMove->getX() == 7))
    {
        if ((possibleMove->getY() == 0) ||
            (possibleMove->getY() == 7))
        { // corners! yay!
            return CORNER_MULT;
        }
        else if ((possibleMove->getY() == 1) ||
                 (possibleMove->getY() == 6))
        { // edges adjacent to corners
            return CORNER_ADJ_EDGE_MULT;
        }
        else
        { // edges not adjacent to corners
            return MID_EDGE_MULT;
        }
    }
    else if ((possibleMove->getY() == 0) ||
             (possibleMove->getY() == 7))
    {
        if ((possibleMove->getX() == 1) ||
            (possibleMove->getX() == 6))
        { // edges adjacent to corners
            return CORNER_ADJ_EDGE_MULT;
        }
        else if ((possibleMove->getX() != 0) &&
                 (possibleMove->getX() != 7))
        { // edges not adjacent to corners
            return MID_EDGE_MULT;
        }
        else
        {
            std::cerr << "Uh oh! This line should never be reached." << std::endl;
            return 1;
        }
    }
    // diagonally adjacent to corner spaces
    else if ((possibleMove->getX() == 1) ||
             (possibleMove->getX() == 6))
    {
        if ((possibleMove->getY() == 1) ||
            (possibleMove->getY() == 6))
        {
            return DIAG_ADJ_CORNER_MULT;
        }
        else return 1; // this is a normal middle space
    }
    // if not one of these special squares, the score should not be modified.
    else return 1;
}

/* This function computes the score for a given move based on how many stones
 * would be converted with the move and the position on the board of the move.
 */
float ExamplePlayer::getScore(Move* possibleMove)
{
    // first find out how many stones we would gain
    
    // create a test board and try out possibleMove on it
    Board* testBoard = new Board();
    testBoard = this->board->copy();
    testBoard->doMove(possibleMove, this->side);
    
    // the difference in the number of stones for our side between the testBoard
    // with the possible move and the original board without it is the number
    // of stones gained and is our initial crude evaluation of possibleScore.
    
    float possibleScore = testBoard->count(this->side) - this->board->count(this->side) - 1;
    
    delete testBoard;
    
    // next modify possibleScore based on where possibleMove is on the board
    possibleScore = possibleScore*movePositionMod(possibleMove);
    
    std::cerr << "Possible move: (" << possibleMove->getX() << "," << possibleMove->getY() << ")" << std::endl;
    std::cerr << "Possible score:" << possibleScore << std::endl;
    
    return possibleScore;
}

/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    
    // define opponent's side
    Side oppSide;
    (this->side == WHITE) ? oppSide = BLACK : oppSide = WHITE;
    
    // process opp's move
    board->doMove(opponentsMove, oppSide);

    bool canMove = false;        
    Move* possibleMove = new Move(0,0);
    Move* bestMove = new Move(0,0);
    float bestScore = (float) MINSCORE;
    float currScore = 0;
    
    // do our move
    for (int x = 0; x < 8 ; x++)
    {
        possibleMove->setX(x);
        for (int y = 0; y < 8 ; y++)
        {
            possibleMove->setY(y);
            if (board->checkMove(possibleMove, this->side))
            { // valid move
                currScore = this->getScore(possibleMove);
                if (currScore > bestScore)
                {
                    bestScore = currScore;
                    bestMove->setX(x);
                    bestMove->setY(y);
                    canMove = true;
                }
                std::cerr << "Current best score:" << bestScore << std::endl;
            }
        }
    }
    std::cerr << "----------Move chosen.----------" << std::endl;
    
    delete possibleMove;
    
    if (canMove)
    {
        this->board->doMove(bestMove, this->side);
        return bestMove;
    }
    else return NULL;
}
    
